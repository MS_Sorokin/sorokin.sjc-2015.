package com.exposit.oop;

public class Animal {

	protected int age;
	protected String name;
	protected String owner;

	public Animal(int age,String name,String owner){
		this.age=age;
		this.name=name;
		this.owner=owner;
	}
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String eatFood(){
		return this.name+" eats food!";
	}
	
	public String makeNoise(){
		return this.name+" makes noises!";
	}

}
