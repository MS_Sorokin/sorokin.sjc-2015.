package com.exposit.sortings;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BubbleSort {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.print("Please, enter length of array:");
		int count = scanner.nextInt();
		System.out.println("Please, enter the array:");
		int[] array = new int[count];
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		doBubbleSort(array);
		System.out.println("Sorted array:");
		for (int i : array) {
			System.out.print(i + " ");
		}
	}

	private static void doBubbleSort(int[] array) {
		int k = array.length;
		for (int i = 0; i < array.length; i++, k--) {
			for (int j = 0; j < k - 1; j++) {
				if (array[j] > array[j + 1]) {
					int temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				} else
					continue;
			}
		}
	}

}
