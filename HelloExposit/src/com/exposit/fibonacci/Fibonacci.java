package com.exposit.fibonacci;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {

		System.out.print("Enter N:");
		int number = new Scanner(System.in).nextInt();
		fibonacci(number);

	}

	public static void fibonacci(int n) {
		long[] fibArray = new long[n];
		fibArray[0] = 1;
		fibArray[1] = 1;

		for (int i = 2; i < n; i++) {
			fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
		}
		
		for (long i : fibArray) {
			System.out.print(i+" ");
		}

	}
}
